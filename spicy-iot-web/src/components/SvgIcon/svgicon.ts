import * as components from '@element-plus/icons-vue';
import type { App } from 'vue';

export default {
	install: (app: App) => {
		for (const key in components) {
			if (Object.prototype.hasOwnProperty.call(components, key)) {
				const componentConfig = components[key as keyof typeof components];
				app.component(componentConfig.name!, componentConfig);
			}
		}
	},
};
