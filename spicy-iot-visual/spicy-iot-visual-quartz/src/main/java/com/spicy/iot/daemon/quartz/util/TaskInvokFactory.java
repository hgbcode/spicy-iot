package com.spicy.iot.daemon.quartz.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.spicy.iot.daemon.quartz.exception.TaskException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Hccake
 * @version 1.0
 * @date 2019/8/8 15:40 TaskInvok工厂类
 */
@Slf4j
public class TaskInvokFactory {

	/**
	 * 根据对应jobType获取对应 invoker
	 * @param jobType
	 * @return
	 * @throws TaskException
	 */
	public static ITaskInvok getInvoker(String jobType) throws TaskException {
		if (StrUtil.isBlank(jobType)) {
			log.info("获取TaskInvok传递参数有误，jobType:{}", jobType);
			throw new TaskException("");
		}

		for (String beanName : SpringUtil.getBeanNamesForType(ITaskInvok.class)) {
			ITaskInvok iTaskInvok = SpringUtil.getBean(beanName);
			if(iTaskInvok.status().equals(jobType)) {
				return iTaskInvok;
			}
		}

		log.info("定时任务类型无对应反射方式，反射类型:{}", jobType);
		throw new TaskException("");
	}

}
