package com.spicy.iot.codegen.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * boolean 类型枚举
 * @author lengleng
 */

@Getter
@RequiredArgsConstructor
public enum BoolFillEnum {

	/**
	 * true
	 */
	TRUE("1"),
	/**
	 * false
	 */
	FALSE("0");

	private final String value;

}
