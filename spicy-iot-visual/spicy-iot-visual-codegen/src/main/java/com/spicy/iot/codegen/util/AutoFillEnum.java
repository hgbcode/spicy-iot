package com.spicy.iot.codegen.util;

/**
 * 字段自动填充 枚举
 *
 * @author 阿沐 babamu@126.com
 */
public enum AutoFillEnum {
	/**
	 * 类型
	 */
	DEFAULT, INSERT, UPDATE, INSERT_UPDATE, CREATE;

}
