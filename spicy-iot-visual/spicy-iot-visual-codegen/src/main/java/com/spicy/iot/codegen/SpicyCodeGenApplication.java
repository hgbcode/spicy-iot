/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.spicy.iot.codegen;

import cn.hutool.core.lang.Console;
import cn.hutool.system.SystemUtil;
import com.spicy.iot.common.datasource.annotation.EnableDynamicDataSource;
import com.spicy.iot.common.feign.annotation.EnableSpicyFeignClients;
import com.spicy.iot.common.security.annotation.EnableSpicyResourceServer;
import com.spicy.iot.common.swagger.annotation.EnableSpicyDoc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author lengleng
 * @date 2018/07/29 代码生成模块
 */
@Slf4j
@EnableDynamicDataSource
@EnableSpicyFeignClients
@EnableSpicyDoc("gen")
@EnableDiscoveryClient
@EnableSpicyResourceServer
@SpringBootApplication
public class SpicyCodeGenApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpicyCodeGenApplication.class, args);
		Environment environment = context.getBean(Environment.class);
		Console.log("============启动*成功============");
		Console.log("JAVA:\n {}", SystemUtil.getJavaInfo());
		Console.log("系统信息:\n {}", SystemUtil.getOsInfo());
		Console.log("网络地址信息:\n {}",SystemUtil.getHostInfo());
		Console.log("运行内存信息：\n {}",SystemUtil.getRuntimeInfo());
		Console.log("======666======running======666======\n" +
				"  \n");
		log.info("访问链接：http://localhost:" + environment.getProperty("local.server.port"));
	}

}
