package com.spicy.iot.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * @author lengleng
 * @date 2018年06月21日 监控中心
 */
@Slf4j
@EnableAdminServer
@EnableDiscoveryClient
@SpringBootApplication
public class SpicyMonitorApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpicyMonitorApplication.class, args);
    }
}
