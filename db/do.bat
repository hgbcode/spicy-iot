:: 直接管理员方式运行即可向host文件添加域名

@echo off
:: 设置控制台编码为 UTF-8，避免中文乱码
chcp 65001

:: 检查是否有管理员权限
openfiles >nul 2>nul
if %errorlevel% neq 0 (
    echo 需要以管理员身份运行脚本!
    pause
    exit /b
)

:: 输入IP地址
set /p IP="请输入要添加的IP地址: "

:: 设置hosts文件的路径
set hostsFile=%SystemRoot%\System32\drivers\etc\hosts

:: 如果用户没有输入IP地址，给出默认提示并退出
if "%IP%"=="" (
    echo 未输入IP地址，脚本将退出。
    pause
    exit /b
)

:: 在开始写入之前，确保hosts文件有换行
echo.>>%hostsFile%

:: 写入数据到hosts文件
echo %IP%   spicy-mysql>>%hostsFile%
echo %IP%   spicy-redis>>%hostsFile%
echo %IP%   spicy-register>>%hostsFile%
echo %IP%   spicy-sentinel>>%hostsFile%
echo %IP%   spicy-seata>>%hostsFile%

echo 127.0.0.1   spicy-monitor>>%hostsFile%
echo 127.0.0.1   spicy-job>>%hostsFile%
echo 127.0.0.1   spicy-gateway>>%hostsFile%

:: 提示操作完成
echo 数据已添加到hosts文件中
pause
