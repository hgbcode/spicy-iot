package com.spicy.iot.common.log.event;

import com.spicy.iot.admin.api.entity.SysLog;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * spring event log
 *
 * @author lengleng
 * @date 2023/8/11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SysLogEventSource extends SysLog {

	/**
	 * 参数重写成object
	 */
	private Object body;

}
