package com.spicy.iot.common.security.filter;

import com.spicy.iot.common.core.util.WebUtils;
import com.spicy.iot.common.security.util.SecurityUtils;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;

import java.io.IOException;

/**
 * log日志输出用户跟IP
 *
 * @author SpicyRabbitLeg
 **/
@Slf4j
@WebFilter(urlPatterns = "/*")
public class MdcFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        try {
            Authentication authentication = SecurityUtils.getAuthentication();
            MDC.put("user", authentication != null ? authentication.getName() : "anonymous");
            MDC.put("ip", WebUtils.getClientIP(httpServletRequest));
            chain.doFilter(httpServletRequest, httpServletResponse);
        } finally {
            // 清空记录
            MDC.clear();
        }
    }
}
