package com.spicy.iot.common.feign;

import com.alibaba.cloud.sentinel.feign.SentinelFeignAutoConfiguration;
import com.spicy.iot.common.feign.core.SpicyFeignInnerRequestInterceptor;
import com.spicy.iot.common.feign.core.SpicyFeignRequestCloseInterceptor;
import com.spicy.iot.common.feign.sentinel.ext.SpicySentinelFeign;
import feign.Feign;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.SpicyFeignClientsRegistrar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;

/**
 * sentinel 配置
 *
 * @author lengleng
 * @date 2020-02-12
 */
@Configuration(proxyBeanMethods = false)
@Import(SpicyFeignClientsRegistrar.class)
@AutoConfigureBefore(SentinelFeignAutoConfiguration.class)
public class SpicyFeignAutoConfiguration {
    @Bean
    @Scope("prototype")
    @ConditionalOnMissingBean
    @ConditionalOnProperty(name = "feign.sentinel.enabled")
    public Feign.Builder feignSentinelBuilder() {
        return SpicySentinelFeign.builder();
    }

    /**
     * add http connection close header
     * @return
     */
    @Bean
    public SpicyFeignRequestCloseInterceptor pigFeignRequestCloseInterceptor() {
        return new SpicyFeignRequestCloseInterceptor();
    }

    /**
     * add inner request header
     * @return PigFeignInnerRequestInterceptor
     */
    @Bean
    public SpicyFeignInnerRequestInterceptor pigFeignInnerRequestInterceptor() {
        return new SpicyFeignInnerRequestInterceptor();
    }
}
