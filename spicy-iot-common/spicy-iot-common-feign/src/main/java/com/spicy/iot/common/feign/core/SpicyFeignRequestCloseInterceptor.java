package com.spicy.iot.common.feign.core;

import feign.RequestInterceptor;
import org.springframework.http.HttpHeaders;

/**
 * @author lengleng
 * @date 2024/3/15
 * <p>
 * http connection close
 */
public class SpicyFeignRequestCloseInterceptor implements RequestInterceptor {

    /**
     * set connection close
     * @param template template
     */
    @Override
    public void apply(feign.RequestTemplate template) {
        template.header(HttpHeaders.CONNECTION, "close");
    }

}