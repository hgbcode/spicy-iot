/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spicy.iot.auth;

import cn.hutool.core.lang.Console;
import cn.hutool.system.SystemUtil;
import com.spicy.iot.common.feign.annotation.EnableSpicyFeignClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author lengleng
 * @date 2018年06月21日 认证授权中心
 */
@Slf4j
@EnableSpicyFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class SpicyAuthApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpicyAuthApplication.class, args);
		Environment environment = context.getBean(Environment.class);
		Console.log("============启动*成功============");
		Console.log("JAVA:\n {}", SystemUtil.getJavaInfo());
		Console.log("系统信息:\n {}", SystemUtil.getOsInfo());
		Console.log("网络地址信息:\n {}",SystemUtil.getHostInfo());
		Console.log("运行内存信息：\n {}",SystemUtil.getRuntimeInfo());
		Console.log("======666======running======666======\n" +
				"  \n");
		log.info("访问链接：http://localhost:" + environment.getProperty("local.server.port"));
	}

}
