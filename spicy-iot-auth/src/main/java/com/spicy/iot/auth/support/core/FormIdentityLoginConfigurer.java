package com.spicy.iot.auth.support.core;


import com.spicy.iot.auth.support.handler.FormAuthenticationFailureHandler;
import com.spicy.iot.auth.support.handler.SsoLogoutSuccessHandler;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;

/**
 * @author lengleng
 * @data 2022-06-04
 *
 * 基于授权码模式 统一认证登录 spring security & sas 都可以使用 所以抽取成 HttpConfigurer
 */
public final class FormIdentityLoginConfigurer extends AbstractHttpConfigurer<FormIdentityLoginConfigurer, HttpSecurity> {

	@Override
	public void init(HttpSecurity http) throws Exception {
		http.formLogin(form -> {
			// 登入界面
			form.loginPage("/token/login");
			// 登入接口
			form.loginProcessingUrl("/token/form");
			// 登入异常返回
			form.failureHandler(new FormAuthenticationFailureHandler());
		});

		http.logout(logout -> {
			// 登出成功后返回json
			logout.logoutSuccessHandler(new SsoLogoutSuccessHandler());
			logout.deleteCookies("JSESSIONID");
			// SSO登出成功处理
			logout.invalidateHttpSession(true);
		});

		http.csrf(AbstractHttpConfigurer::disable);
	}

}
