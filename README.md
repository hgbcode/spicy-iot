## 系统说明

- 基于 Spring Cloud 、Spring Boot、 OAuth2 的 RBAC **企业快速开发平台**， 同时支持微服务架构和单体架构
- 提供对 Spring Authorization Server 生产级实践，支持多种安全授权模式
- 提供对常见容器化方案支持 Kubernetes、Rancher2 、Kubesphere、EDAS、SAE 支持

#### 分支说明

- jdk17: java17/21 + springboot 3.3 + springcloud 2023

## 快速开始

### 核心依赖

| 依赖                         | 版本       |
|-----------------------------|------------|
| Spring Boot                 | 3.3.2      |
| Spring Cloud                | 2023.0.3   |
| Spring Cloud Alibaba        | 2023.0.1.2 |
| Spring Authorization Server | 1.3.1      |
| Mybatis Plus                | 3.5.7      |
| Vue                         | 3.4        |
| Element Plus                | 2.7        |
| NACOS                       | 2.4.1      |

### 模块说明

```lua
spicy-iot
├── spicy-iot-boot -- 单体模式启动器[9999]
├── spicy-iot-auth -- 授权服务提供[3000]
└── spicy-iot-common -- 系统公共模块
     ├── spicy-iot-common-bom -- 全局依赖管理控制
     ├── spicy-iot-common-core -- 公共工具类核心包
     ├── spicy-iot-common-datasource -- 动态数据源包
     ├── spicy-iot-common-log -- 日志服务
     ├── spicy-iot-common-oss -- 文件上传工具类
     ├── spicy-iot-common-mybatis -- mybatis 扩展封装
     ├── spicy-iot-common-seata -- 分布式事务
     ├── spicy-iot-common-security -- 安全工具类
     ├── spicy-iot-common-swagger -- 接口文档
     ├── spicy-iot-common-feign -- feign 扩展封装
     └── spicy-iot-common-xss -- xss 安全封装
├── spicy-iot-register -- Nacos Server
├── spicy-iot-gateway -- Spring Cloud Gateway网关
└── spicy-iot-upms -- 通用用户权限管理模块
     └── spicy-iot-upms-api -- 通用用户权限管理系统公共api模块
     └── spicy-iot-upms-biz -- 通用用户权限管理系统业务处理模块
└── spicy-iot-visual
     └── spicy-iot-visual-monitor -- 服务监控
     ├── spicy-iot-visual-codegen -- 图形化代码生成
     └── spicy-iot-visual-quartz -- 定时任务管理台
```
## 开源共建

### 开源协议

spicy-iot 开源软件遵循 [Apache 2.0 协议](https://www.apache.org/licenses/LICENSE-2.0.html)。
允许商业使用，但务必保留类作者、Copyright 信息。

![](https://minio.pigx.top/oss/1655474288.jpg)
