package com.spicy.iot.gateway;

import cn.hutool.core.lang.Console;
import cn.hutool.system.SystemUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author SpicyRabbitLeg
 **/
@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
public class SpicyGatewayApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpicyGatewayApplication.class, args);
        Environment environment = context.getBean(Environment.class);
        Console.log("============启动*成功============");
        Console.log("JAVA:\n {}", SystemUtil.getJavaInfo());
        Console.log("系统信息:\n {}", SystemUtil.getOsInfo());
        Console.log("网络地址信息:\n {}",SystemUtil.getHostInfo());
        Console.log("运行内存信息：\n {}",SystemUtil.getRuntimeInfo());
        Console.log("======666======running======666======\n" +
                "  \n");
        log.info("访问链接：http://localhost:" + environment.getProperty("local.server.port"));
    }
}
