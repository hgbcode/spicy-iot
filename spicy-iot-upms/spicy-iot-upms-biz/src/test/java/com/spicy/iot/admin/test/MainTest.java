package com.spicy.iot.admin.test;

import com.spicy.iot.admin.util.SmsClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author SpicyRabbitLeg
 **/
@SpringBootTest
public class MainTest {

    @Test
    public void testSendMessage() {
        boolean result = SmsClient.sendMessage("19970380062",String.format("【交投大唐】测试你好，监测到贵公司测试日与浙江大唐智慧产业有限公司签订的代采业务合同中（合同编号为测试，规格型号为测试，采购价为测试元）的货物价格已经首次跌破验证码%s元，请及时补充保证金或补充货物入仓。","19901"));
        System.out.println(result);
    }
}
