package com.spicy.iot.admin.util;

import cn.hutool.core.date.SystemClock;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.spicy.iot.common.core.constant.StringConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * 短信发送工具类
 *
 * @author 泥鳅压滑板
 */
@Slf4j
@Component
public class SmsClient {
    @Value("${sms.phone.api}")
    private String api;
    @Value("${sms.phone.app-key}")
    private String appKey;
    @Value("${sms.phone.app-secret}")
    private String appSecret;
    @Value("${sms.phone.send}")
    private String send;
    @Value("${sms.phone.balance}")
    private String balance;

    private Map<String, Object> createParams(String mobile) {
        HashMap<String, Object> params = new HashMap<>(4);
        String timestamp = String.valueOf(SystemClock.now());
        params.put(StringConstants.Prefix.APP_KEY, appKey);
        params.put(StringConstants.Prefix.APP_SECRET, getMd532Str(appSecret + mobile + timestamp));
        params.put(StringConstants.Type.TIMESTAMP, timestamp);
        return params;
    }

    /**
     * 发送短信
     *
     * @param mobile  手机号
     * @param content 消息内容
     * @return 发送是否成功
     */
    private boolean send(String[] mobile, String content) {
        String join = String.join(",", mobile);
        Map<String, Object> params = createParams(join);
        params.put("mobile", join);
        params.put("content", content);

        String post = HttpUtil.post(api + send, params);
        JSONObject parseObj = JSON.parseObject(post);
        log.info("send message phoneNumber:{}  result:{}", join, post);
        return "0".equals(parseObj.getString("code"));
    }

    /**
     * 校验是否还有余额
     *
     * @return 余额
     */
    private String check() {
        Map<String, Object> params = createParams("");
        String post = HttpUtil.post(api + balance, params);
        JSONObject parseObj = JSON.parseObject(post);
        log.info("请求返回结果:{} ", post);
        return parseObj.getString("balance");
    }


    private static String getMd532Str(String sourceStr) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(sourceStr.getBytes());
            byte[] b = md.digest();
            int i;
            StringBuilder buf = new StringBuilder();
            for (byte value : b) {
                i = value;
                if (i < 0) {
                    i += 256;
                }
                if (i < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 发送短信
     *
     * @param mobile  手机号列表
     * @param content 内容
     * @return 成功否
     */
    public static boolean sendMessage(String[] mobile, String content) {
        return SpringUtil.getBean(SmsClient.class).send(mobile, content);
    }

    /**
     * 发送短信
     *
     * @param mobile  手机号
     * @param content 内容
     * @return 成功否
     */
    public static boolean sendMessage(String mobile, String content) {
        return sendMessage(new String[]{mobile}, content);
    }
}
